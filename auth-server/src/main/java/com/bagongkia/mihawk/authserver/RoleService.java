package com.bagongkia.mihawk.authserver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

	public String[] getRoles() throws Exception {
        List<String> roles = new ArrayList<>();
        roleRepository.findAll().forEach(role -> {
            roles.add(role.getCode());
        });
        
        return roles.toArray(new String[0]);
	}

}