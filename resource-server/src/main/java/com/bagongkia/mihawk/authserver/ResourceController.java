package com.bagongkia.mihawk.authserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ResourceController {

    @RequestMapping("/v1/hello")
    public Map<String, Object> hello(){
        Map<String, Object> hasil = new HashMap<String, Object>();
        hasil.put("success", Boolean.TRUE);
        hasil.put("message", "Hello Mihawk");
        return hasil;
    }
}